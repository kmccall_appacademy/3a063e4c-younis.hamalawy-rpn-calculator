class RPNCalculator < Array
  attr_accessor :calculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_op(:+)
  end

  def minus
    perform_op(:-)
  end

  def divide
    perform_op(:/)
  end

  def times
    perform_op(:*)
  end

  def tokens(string)
    tokens = string.split
    tokens.map { |ch| operation?(ch) ? ch.to_sym : Integer(ch) }
  end

  def evaluate(string)
    tokens = tokens(string)

    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_op(token)
      end
    end
    value
  end

  private

  def operation?(ch)
    [:+, :-, :*, :/].include?(ch.to_sym)
  end

  def perform_op(symbol_op)
    raise "calculator is empty" if @stack.size < 2
    second_op = @stack.pop
    first_op = @stack.pop

    case symbol_op
    when (:+)
      @stack << first_op + second_op
    when (:-)
      @stack << first_op - second_op
    when (:*)
      @stack << first_op * second_op
    when (:/)
      @stack << first_op.fdiv(second_op)
    else
      @stack << first_op
      @stack << second_op
      raise "No such operation: #{symbol_op}"
    end


  end

end
